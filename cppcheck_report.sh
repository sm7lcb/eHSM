#!/bin/bash
xml_report_name=cppcheck_report.xml
report_path=reports/cppcheck
if [ -f build/$xml_report_name ]; then
    cp build/$xml_report_name $report_path/$xml_report_name
    cppcheck-htmlreport --file=$report_path/$xml_report_name --report-dir=$report_path --source-dir=.
else
    echo "ERROR build/$xml_report_name not found"
    exit 1
fi
