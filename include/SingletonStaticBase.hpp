/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef SINGLETONSTATICBASE_HPP
#define SINGLETONSTATICBASE_HPP
#include "Utils.hpp"

namespace eHSM
{
/**
 * @brief The SingletonStaticBase class
 * Provides a way of make all public member of a class accesiblel from
 * everywhere. Used to keep a class
 * @tparam Type Data type of the array.
 */
template <class Type>
class SingletonStaticBase
{
 public:
  static Type &instance();
  SingletonStaticBase(Type *instancePtr)
  {
    if(instancePtr_ == E_NULLPTR)
    {
      instancePtr_ = static_cast<Type *>(instancePtr);
    }
  }

 private:
  static Type *instancePtr_;
  E_DISABLE_COPY(SingletonStaticBase);
};

template <class Type>
Type *SingletonStaticBase<Type>::instancePtr_ = E_NULLPTR;

template <class Type>
Type &SingletonStaticBase<Type>::instance()
{
  return *instancePtr_;
}

}  // namespace eHSM

#endif  // SINGLETONSTATICBASE_HPP
