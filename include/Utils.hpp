/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef UTILS_HPP
#define UTILS_HPP

#include <cstddef>
#include <cstdint>
namespace eHSM
{
#if __cplusplus >= 201103L  // c++11 support
#  define E_COMPILER_NULLPTR
#  define E_COMPILER_DEFAULT_MEMBERS
#  define E_COMPILER_DELETE_MEMBERS
#  define E_COMPILER_OVERRIDE_MEMBERS
#  define E_COMPILER_FINAL_MEMBERS
#  define E_COMPILER_CONSTEXPR_MEMBERS
#endif

#ifdef E_COMPILER_NULLPTR
#  define E_NULLPTR nullptr
#else
#  define E_NULLPTR NULL
#endif

#ifdef E_COMPILER_DEFAULT_MEMBERS
#  define E_DECLARE_DEFAULT = default
#else
#  define E_DECLARE_DEFAULT
#endif

#ifdef E_COMPILER_DELETE_MEMBERS
#  define E_DECLARE_DELETE = delete
#else
#  define E_DECLARE_DELETE
#endif

#ifdef E_COMPILER_OVERRIDE_MEMBERS
#  define E_DECLARE_OVERRIDE override
#else
#  define E_DECLARE_OVERRIDE
#endif

#ifdef E_COMPILER_FINAL_MEMBERS
#  define E_DECLARE_FINAL final
#else
#  define E_DECLARE_FINAL
#endif

#ifdef E_COMPILER_CONSTEXPR_MEMBERS
#  define E_DECLARE_CONSTEXPR constexpr
#else
#  define E_DECLARE_CONSTEXPR
#endif

/*
       Utility macros and inline functions
*/
template <typename T>
E_DECLARE_CONSTEXPR inline T Abs(const T &t)
{
  return t >= 0 ? t : -t;
}

E_DECLARE_CONSTEXPR inline int Round(float d)
{
  return d >= 0.0f ? int(d + 0.5f) : int(d - float(int(d - 1)) + 0.5f) + int(d - 1);
}

template <typename T>
E_DECLARE_CONSTEXPR inline const T &Min(const T &a, const T &b)
{
  return (a < b) ? a : b;
}

template <typename T>
E_DECLARE_CONSTEXPR inline const T &Max(const T &a, const T &b)
{
  return (a < b) ? b : a;
}

template <typename T>
E_DECLARE_CONSTEXPR inline const T &Bound(const T &min, const T &val, const T &max)
{
  return Max(min, Min(max, val));
}

#define E_DECLARE_UNUSED(x) ((void)(x))

#define E_DISABLE_COPY(Class)            \
  Class(const Class &) E_DECLARE_DELETE; \
  Class &operator=(const Class &) E_DECLARE_DELETE
}  // namespace eHSM
#endif  // UTILS_HPP
