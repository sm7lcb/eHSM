/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef DMABUFFER_H
#define DMABUFFER_H

#include <assert.h>

#include <cstdint>
#include <cstring>

#include "Utils.hpp"

namespace eHSM
{
/**
 * @brief The LinearBuffer class
 * Provides a buffer of items, the items can't be directly indexed, can only be
 * writted at the end. For reading it must use the pointer to the internal
 * array.
 * @tparam Type Items data type, default to std::uint8_t
 */
template <typename Type>
class DmaBuffer
{
 public:
  /**
   * @brief reset Empty the buffer
   */
  void reset()
  {
    writeCounter_ = 0;
  }

  /**
   * @brief write
   * Writes an Item to the end of the buffer
   * @param item Item's reference
   * @return
   * 1 if OK
   * 0 if buffer full
   *
   */
  std::size_t write(const Type &data)
  {
    if(isFull())
    {
      return 0;
    }
    bufferPtr_[writeCounter_++] = data;  // Guarda el dato.
    return 1;
  }

  /**
   * @brief write Writes several items in to the buffer's end.
   * @param items Pointer to the items.
   * @param itemsToWrite Number of items to write.
   * @return
   * Number of items written
   * 0 if buffer full
   */
  std::size_t write(const Type *data, std::size_t dataLen)
  {
    if(data == E_NULLPTR || dataLen == 0)
    {
      return 0;
    }
    std::size_t itemsToWrite = (dataLen <= spaceLeft()) ? dataLen : spaceLeft();
    std::memcpy(bufferPtr_ + writeCounter_, data, (std::size_t)(sizeof(Type) * itemsToWrite));
    writeCounter_ += itemsToWrite;
    return itemsToWrite;
  }

  /**
   * @brief at
   * @param pos Position of the item in the array.
   * @return A reference to the item.
   */
  Type &at(std::size_t pos) const
  {
    assert(pos < bufferSize_);
    assert(pos < writeCounter_);
    return bufferPtr_[pos];
  }
  /**
   * @brief size
   * @return
   * The size of the buffer.
   */
  std::size_t size() const
  {
    return bufferSize_;
  }

  /**
   * @brief itemsAviable
   * @return
   * Number if items for read in the buffer.
   */
  std::size_t itemsAviable() const
  {
    return writeCounter_;
  }

  /**
   * @brief spaceLeft
   * @return
   * Number of items that can be written.
   */
  std::size_t spaceLeft() const
  {
    return (bufferSize_ - writeCounter_);
  }

  /**
   * @brief data
   * @return a const pointer to the internal array
   */
  const Type *data() const
  {
    return bufferPtr_;
  }

  /**
   * @brief data
   * @return a pointer to the internal array
   */
  Type *data()
  {
    return bufferPtr_;
  }

  /**
   * @brief at
   * @param pos Position of the item in the array.
   * @return A reference to the item.
   */
  Type &at(std::size_t pos)
  {
    assert(pos < bufferSize_);
    return bufferPtr_[pos];
  }

  /**
   * @brief isFull
   * @return
   * True if buffer full.
   * False if not full.
   */
  bool isFull()
  {
    return (itemsAviable() == bufferSize_);
  }

  /**
   * @brief isEmpty
   * @return
   * True if buffe empty.
   * False if not empty.
   */
  bool isEmpty()
  {
    return (itemsAviable() == 0);
  }

  /**
   * @brief setItemsAviable
   * Set the current items in the buffer value
   * @param qtty
   * @retval true if qtty <= size()
   * @retval false if qtty > size()
   * @warning It's to the user responsability to be sure of what is doing
   */
  bool setItemsAviable(std::size_t qtty)
  {
    if(qtty <= size())
    {
      writeCounter_ = qtty;
      return true;
    }
    return false;
  }

 protected:
  /**
   * @brief LinearBuffer
   * Protected Constructor, for be used for the derived class.
   * @param bufferPtr Pointer to the derived class internal buffer.
   * @param buffSize Derived class buffer sizes.
   */
  DmaBuffer(Type *bufferPtr, std::size_t buffSize) : bufferPtr_(bufferPtr), bufferSize_(buffSize), writeCounter_(0) {}
  Type *      bufferPtr_;
  std::size_t bufferSize_;
  std::size_t writeCounter_;

 private:
  DmaBuffer(DmaBuffer &);
  DmaBuffer &operator=(DmaBuffer &);
};

namespace Declare
{
/**
 * @brief Declare::LinearBuffer class
 * Class used to instanciate the objects
 * @tparam Type Items data type, default to std::uint8_t
 * @tparam BUFFER_SIZE buffer size
 */
template <typename Type, std::size_t BUFFER_SIZE = 256>
class DmaBuffer : public ::eHSM::DmaBuffer<Type>
{
 public:
  /**
   * @brief RingBuffer
   * Public constructor
   */
  DmaBuffer() : eHSM::DmaBuffer<Type>(buffer_, BUFFER_SIZE) {}

 private:
  Type buffer_[BUFFER_SIZE];
  E_DISABLE_COPY(DmaBuffer);
};
}  // namespace Declare
}  // namespace eHSM
#endif  // DMABUFFER_H
