/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef RINGBUFFER_HPP
#define RINGBUFFER_HPP
#include <assert.h>

#include <cstdint>
#include <cstring>

#include "Utils.hpp"

namespace eHSM
{
/**
 * @class RingBuffer
 * Generic, lockfree, non redimentionable and non copiable Ring Buffer class,
 * the buffer is allocated in the stack
 * @tparam Type Specify the buffer data type.
 */
template <typename Type = std::uint8_t>
class RingBuffer
{
 public:
  /**
   * @brief write
   * Writes an Item to the end of the buffer
   * @param item Item's reference
   * @return
   * 1 if OK
   * 0 if buffer full
   *
   */
  std::size_t write(const Type &itemRef)
  {
    if(isFull())
    {
      return 0;  // Buffer Full.
    }
    _bufferPtr[_writeIndex++] = itemRef;  // Guarda el dato.
    _writeCounter++;                      // Incrementa el contador de elemetos.
    if(_writeIndex == _bufferSize)
    {
      _writeIndex = 0;  // Comienza a escribir desde el principio.
    }
    return 1;
  }

  /**
   * @brief write Writes several items in to the buffer's end.
   * @param items Pointer to the items.
   * @param itemsToWrite Number of items to write.
   * @return
   * Number of items written
   * 0 if buffer full
   */
  std::size_t write(const Type *itemsPtr, std::uint32_t itemsSize)
  {
    if(itemsPtr == E_NULLPTR || itemsSize == 0 || isFull())
    {
      return 0;
    }
    std::size_t itemsToWrite = itemsSize <= spaceLeft() ? itemsSize : spaceLeft();
    bool        linearWrite  = (_bufferSize - _writeIndex) >= itemsToWrite;
    if(linearWrite)
    {
      std::memcpy(_bufferPtr + _writeIndex, itemsPtr, (std::size_t)(sizeof(Type) * itemsToWrite));
      _writeIndex += itemsToWrite;
      if(_writeIndex >= _bufferSize)
      {
        _writeIndex = 0;  // Comienza a escribir desde el principio.
      }
    }
    else
    {
      // no hay espacio suficiente para guardar el bloque de forma continua
      std::size_t itemsUntilEnd = _bufferSize - _writeIndex;
      std::size_t itemsOffset   = itemsToWrite - itemsUntilEnd;
      // escribe hasta el final del buffer
      std::memcpy(_bufferPtr + _writeIndex, itemsPtr, (std::size_t)(sizeof(Type) * itemsUntilEnd));
      // comienza desde el principio de buffer_
      std::memcpy(_bufferPtr, itemsPtr + itemsUntilEnd, (std::size_t)(sizeof(Type) * itemsOffset));
      _writeIndex = itemsOffset;
    }
    _writeCounter += itemsToWrite;
    return itemsToWrite;
  }

  /**
   * @brief read Reads first Item
   * @param rData
   * @return
   * 0 if buffer empty
   * 1 if ok
   */
  std::size_t read(Type &itemRef)
  {
    if(isEmpty())
    {
      return 0;
    }
    itemRef = _bufferPtr[_readIndex++];  // Extrae el dato.
    _readCounter++;
    if(_readIndex >= _bufferSize)
    {
      _readIndex = 0;  // Comienza a leer desde el principio.
    }
    return 1;
  }

  /**
   * @brief read Reads several items from the buffer.
   * @param pData Pointer to the destination buffer.
   * @param itemsToRead Number of items to read.
   * @return
   * Number of items readed
   */
  std::size_t read(Type *itemsPtr, std::uint32_t itemsSize)
  {
    if(itemsPtr == E_NULLPTR || itemsSize == 0 || isEmpty())
    {
      return 0;
    }

    std::size_t itemsToRead = itemsSize <= itemsAviable() ? itemsSize : itemsAviable();
    bool        linearRead  = (_bufferSize - _readIndex) > itemsToRead;
    if(linearRead)
    {
      // si se pueden leer de forma continua
      std::memcpy(itemsPtr, _bufferPtr + _readIndex, (std::size_t)(sizeof(Type) * itemsToRead));
      _readIndex += itemsToRead;
      if(_readIndex >= _bufferSize)
      {
        _readIndex = 0;  // Comienza a leer desde el principio.
      }
    }
    else
    {
      // no se pueden leer de forma continua
      std::size_t itemsUntilEnd = _bufferSize - _readIndex;
      std::size_t itemsOffSet   = itemsToRead - itemsUntilEnd;
      // lee hasta el final del buffer
      std::memcpy(itemsPtr, _bufferPtr + _readIndex, (std::size_t)(sizeof(Type) * itemsUntilEnd));
      // comienza desde el principio de buffer_
      std::memcpy(itemsPtr + itemsUntilEnd, _bufferPtr, (std::size_t)(sizeof(Type) * itemsOffSet));
      _readIndex = itemsOffSet;
    }
    _readCounter += itemsToRead;
    return itemsToRead;
  }

  /**
   * @brief at
   * @param pos Position of the item in the array.
   * @return A reference to the item.
   */
  Type &at(std::size_t pos) const
  {
    assert(pos < _bufferSize);
    return _bufferPtr[pos];
  }

  /**
   * @brief isFull
   * @retval true if buffer full.
   * @retval false if not full.
   */
  bool isFull() const
  {
    return (itemsAviable() == _bufferSize);
  }

  /**
   * @brief isEmpty
   * @retval true if buffe empty.
   * @retval false if not empty.
   */
  bool isEmpty() const
  {
    return (itemsAviable() == 0);
  }

  /**
   * @brief itemsAviable
   * @return
   * Number if items for read in the buffer.
   */
  std::size_t itemsAviable() const
  {
    return (_writeCounter - _readCounter);
  }

  /**
   * @brief size
   * @return
   * The size of the buffer.
   */
  std::size_t size() const
  {
    return _bufferSize;
  }

  /**
   * @brief spaceLeft
   * @return
   * Number of items that can be written.
   */
  std::size_t spaceLeft() const
  {
    return (_bufferSize - itemsAviable());
  }

  /**
   * @brief peekHead reads but not removes the first item in the buffer
   * @param rData
   * @return
   * True if ok
   * False if buffer empty.
   */
  bool peekFirst(Type &itemRef) const
  {
    if(isEmpty())
    {
      return false;
    }

    itemRef = _bufferPtr[_readIndex];
    return true;
  }

  /**
   * @brief peekLast reads but not removes the last item in the buffer
   * @param rData
   * @return
   * True if ok
   * False if buffer empty.
   */
  bool peekLast(Type &itemRef) const
  {
    if(isEmpty())
    {
      return false;
    }
    if(_writeIndex != 0)
    {
      itemRef = _bufferPtr[_writeIndex - 1];
    }
    else
    {
      // if writeIndex == 0 the last written item location it's bufferSize -1
      itemRef = _bufferPtr[_bufferSize - 1];
    }
    return true;
  }

  /**
   * @brief reset Empty the buffer
   */
  void reset()
  {
    _writeIndex   = 0;
    _readIndex    = 0;
    _readCounter  = 0;
    _writeCounter = 0;
  }

 protected:
  RingBuffer(Type *bufferPtr, std::size_t buffSize)
      : _bufferPtr(bufferPtr), _bufferSize(buffSize), _readIndex(0), _writeIndex(0), _readCounter(0), _writeCounter(0)
  {
  }

  Type *               _bufferPtr;
  std::size_t          _bufferSize;
  std::size_t          _readIndex;   //!< always point to the first item in the FIFO.
  std::size_t          _writeIndex;  //!< always point to the next free slot in the FIFO.
  volatile std::size_t _readCounter;
  volatile std::size_t _writeCounter;

 private:
  RingBuffer(RingBuffer &);
  RingBuffer &operator=(RingBuffer &);
};

namespace Declare
{
/**
 * @class RingBuffer
 * Generic, lockfree, non redimentionable and non copiable Ring Buffer class,
 * the buffer is allocated in the stack
 * @tparam Type Specify the buffer data type.
 * @tparam BUFFER_SIZE static allocated buffer size
 */
template <typename Type = std::uint8_t, std::size_t BUFFER_SIZE = 256>
class RingBuffer : public ::eHSM::RingBuffer<Type>
{
 public:
  /**
   * @brief RingBuffer
   * Public constructor
   */
  RingBuffer() : eHSM::RingBuffer<Type>(_bufferPtr, BUFFER_SIZE) {}

 private:
  Type _bufferPtr[BUFFER_SIZE];
  RingBuffer(RingBuffer &);
  RingBuffer &operator=(RingBuffer &);
};
}  // namespace Declare
}  // namespace eHSM

#endif  // RINGBUFFER_HPP
