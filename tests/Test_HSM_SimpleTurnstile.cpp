/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <cstdint>
#include <cstring>

#define MAX_NEST_DEPTH 5

#include "HierarchicalStateMachine.hpp"

const int STATE_MAX_EVENTS = 2;

using namespace eHSM;

class Test_HSM_Turnstile : public ::testing::Test
{
 public:
  enum Events
  {
    CoinInserted,
    BarPushed
  };
  Declare::State<STATE_MAX_EVENTS> locked;
  Declare::State<STATE_MAX_EVENTS> unlocked;
  HierarchicalStateMachine         hsm;
};

TEST_F(Test_HSM_Turnstile, CheckInitialState)
{
  hsm.setInitialState(locked);
  EXPECT_EQ(hsm.initialState(), &locked);
}

TEST_F(Test_HSM_Turnstile, CheckCurrentState)
{
  hsm.setInitialState(locked);
  EXPECT_EQ(hsm.initialState(), &locked);
  EXPECT_TRUE(hsm.start());
  EXPECT_EQ(hsm.currentState(), &locked);
}

TEST_F(Test_HSM_Turnstile, CheckStateTransition)
{
  hsm.setInitialState(locked);
  locked.addEvent(CoinInserted, unlocked);
  EXPECT_TRUE(hsm.start());
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(CoinInserted);
  EXPECT_EQ(hsm.currentState(), &unlocked);
}

TEST_F(Test_HSM_Turnstile, CheckNotTransition)
{
  hsm.setInitialState(locked);
  locked.addEvent(CoinInserted, unlocked);
  EXPECT_TRUE(hsm.start());
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(BarPushed);
  EXPECT_EQ(hsm.currentState(), &locked);
}

TEST_F(Test_HSM_Turnstile, CheckAutotransition)
{
  hsm.setInitialState(locked);
  locked.addEvent(CoinInserted, unlocked);
  locked.addEvent(BarPushed, locked);
  EXPECT_TRUE(hsm.start());
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(BarPushed);
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(BarPushed);
  EXPECT_EQ(hsm.currentState(), &locked);
}

TEST_F(Test_HSM_Turnstile, CheckTurnstile)
{
  hsm.setInitialState(locked);
  locked.addEvent(CoinInserted, unlocked);
  unlocked.addEvent(BarPushed, locked);
  EXPECT_TRUE(hsm.start());
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(BarPushed);
  EXPECT_EQ(hsm.currentState(), &locked);
  hsm.dispatch(CoinInserted);
  EXPECT_EQ(hsm.currentState(), &unlocked);
  hsm.dispatch(CoinInserted);
  EXPECT_EQ(hsm.currentState(), &unlocked);
  hsm.dispatch(BarPushed);
  EXPECT_EQ(hsm.currentState(), &locked);
}
