/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <cstdint>
#include <cstring>

#include "Signal.hpp"

bool signalGlobal      = false;
bool signalStatic      = false;
bool signalPublic      = false;
bool signalVirtualBase = false;
bool signalVirtualSon  = false;
bool signalConst       = false;

void globalFunctionSignal(void)
{
  signalGlobal = true;
}

class Base0signal
{
 public:
  static void staticMember(void)
  {
    signalStatic = true;
  }

  void publicMember(void)
  {
    signalPublic = true;
  }
};

class Base1signal
{
 public:
  virtual void virtualMember(void)
  {
    signalVirtualBase = true;
  }
};

class Derivedsignal : public Base0signal, Base1signal
{
 public:
  virtual void virtualMember(void)
  {
    signalVirtualSon = true;
  }
  void constMember(void) const
  {
    signalConst = true;
  }
};

using namespace eHSM;

class SignalVoid6MaxSlots : public ::testing::Test
{
 public:
  Signal<void, 6> signal;
  Base0signal     objBase0;
  Base1signal     objBase1;
  Derivedsignal   objSon;
};

TEST_F(SignalVoid6MaxSlots, EmptyAfterCreation)
{
  EXPECT_TRUE(signal.isEmpty());
}

TEST_F(SignalVoid6MaxSlots, ConnectionSuccessful)
{
  EXPECT_TRUE(signal.connect<&globalFunctionSignal>());
  EXPECT_TRUE(signal.connect<&Base0signal::staticMember>());
  EXPECT_TRUE((signal.connect<Base0signal, &Base0signal::publicMember>)(&objBase0));
  EXPECT_TRUE((signal.connect<Base1signal, &Base1signal::virtualMember>)(&objBase1));
  EXPECT_TRUE((signal.connect<Derivedsignal, &Derivedsignal::virtualMember>)(&objSon));
  EXPECT_TRUE((signal.connect<Derivedsignal, &Derivedsignal::constMember>)(&objSon));
  EXPECT_FALSE(signal.connect<&globalFunctionSignal>());
}

TEST_F(SignalVoid6MaxSlots, NotifySuccessful)
{
  EXPECT_FALSE(signal.notify());
  EXPECT_TRUE(signal.connect<&globalFunctionSignal>());
  EXPECT_TRUE(signal.connect<&Base0signal::staticMember>());
  EXPECT_TRUE((signal.connect<Base0signal, &Base0signal::publicMember>)(&objBase0));
  EXPECT_TRUE((signal.connect<Base1signal, &Base1signal::virtualMember>)(&objBase1));
  EXPECT_TRUE((signal.connect<Derivedsignal, &Derivedsignal::virtualMember>)(&objSon));
  EXPECT_TRUE((signal.connect<Derivedsignal, &Derivedsignal::constMember>)(&objSon));
  EXPECT_TRUE(signal.notify());
  EXPECT_TRUE(signalConst);
  EXPECT_TRUE(signalGlobal);
  EXPECT_TRUE(signalPublic);
  EXPECT_TRUE(signalStatic);
  EXPECT_TRUE(signalVirtualBase);
  EXPECT_TRUE(signalVirtualSon);
}

TEST_F(SignalVoid6MaxSlots, DisconnectAllSlots)
{
  EXPECT_TRUE(signal.connect<&globalFunctionSignal>());
  EXPECT_TRUE(signal.connect<&Base0signal::staticMember>());
  EXPECT_TRUE(signal.notify());
  signal.disconnectAll();
  EXPECT_TRUE(signal.isEmpty());
}

TEST_F(SignalVoid6MaxSlots, DisconnectSlot)
{
  EXPECT_TRUE(signal.connect<&globalFunctionSignal>());
  EXPECT_TRUE(signal.connect<&Base0signal::staticMember>());
  EXPECT_TRUE(signal.notify());
  EXPECT_TRUE(signalGlobal);
  EXPECT_TRUE(signalStatic);
  signal.disconnect<&globalFunctionSignal>();
  signalGlobal = signalStatic = false;
  EXPECT_TRUE(signal.notify());
  EXPECT_TRUE(signalStatic);
  EXPECT_FALSE(signalGlobal);
}
