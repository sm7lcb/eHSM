/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include <gtest/gtest.h>

#include <cstdint>
#include <cstring>

#include "Signal.hpp"

bool TestGlobal         = false;
bool TestStatic         = false;
bool TestPublic         = false;
bool TestVirtualBase    = false;
bool TestVirtualDerived = false;
bool TestConst          = false;

int argGlobal         = 0;
int argStatic         = 0;
int argPublic         = 0;
int argVirtualBase    = 0;
int argVirtualDerived = 0;
int argConst          = 0;

const int TEST_INT_VALUE = 55;

void globalFunction(int number)
{
  TestGlobal = true;
  argGlobal  = number;
}

class Base0
{
 public:
  static void staticMember(int number)
  {
    TestStatic = true;
    argStatic  = number;
  }

  void publicMember(int number)
  {
    TestPublic = true;
    argPublic  = number;
  }
};

class Base1
{
 public:
  virtual void virtualMember(int number)
  {
    TestVirtualBase = true;
    argVirtualBase  = number;
  }
};

class Derived : public Base1
{
 public:
  virtual void virtualMember(int number)
  {
    TestVirtualDerived = true;
    argVirtualDerived  = number;
  }
  void constMember(int number) const
  {
    TestConst = true;
    argConst  = number;
  }
};

using namespace eHSM;

class SignalInt6MaxSlots : public ::testing::Test
{
 public:
  Signal<int, 6> signal;
  Base0          objBase0;
  Base1          objBase1;
  Derived        objDerived;
};

TEST_F(SignalInt6MaxSlots, EmptyAfterCreation)
{
  EXPECT_TRUE(signal.isEmpty());
}

TEST_F(SignalInt6MaxSlots, ConnectionSuccessful)
{
  EXPECT_TRUE(signal.connect<&globalFunction>());
  EXPECT_TRUE(signal.connect<&Base0::staticMember>());
  EXPECT_TRUE((signal.connect<Base0, &Base0::publicMember>)(&objBase0));
  EXPECT_TRUE((signal.connect<Base1, &Base1::virtualMember>)(&objBase1));
  EXPECT_TRUE((signal.connect<Derived, &Derived::virtualMember>)(&objDerived));
  EXPECT_TRUE((signal.connect<Derived, &Derived::constMember>)(&objDerived));
  EXPECT_FALSE(signal.connect<&globalFunction>());  // can only connect 6
                                                    // "slots"
}

TEST_F(SignalInt6MaxSlots, NotifySuccessful)
{
  EXPECT_FALSE(signal.notify(TEST_INT_VALUE));
  EXPECT_TRUE(signal.connect<&globalFunction>());
  EXPECT_TRUE(signal.connect<&Base0::staticMember>());
  EXPECT_TRUE((signal.connect<Base0, &Base0::publicMember>)(&objBase0));
  EXPECT_TRUE((signal.connect<Base1, &Base1::virtualMember>)(&objBase1));
  EXPECT_TRUE((signal.connect<Derived, &Derived::virtualMember>)(&objDerived));
  EXPECT_TRUE((signal.connect<Derived, &Derived::constMember>)(&objDerived));
  EXPECT_TRUE(signal.notify(TEST_INT_VALUE));
  EXPECT_TRUE(TestGlobal);
  EXPECT_TRUE(TestStatic);
  EXPECT_TRUE(TestPublic);
  //    EXPECT_TRUE(TestVirtualBase);
  //    EXPECT_TRUE(TestVirtualDerived);
  EXPECT_TRUE(TestConst);
  EXPECT_EQ(argConst, TEST_INT_VALUE);
  EXPECT_EQ(argGlobal, TEST_INT_VALUE);
  EXPECT_EQ(argPublic, TEST_INT_VALUE);
  EXPECT_EQ(argStatic, TEST_INT_VALUE);
  //    EXPECT_EQ(argVirtualBase, TEST_INT_VALUE);
  //    EXPECT_EQ(argVirtualDerived, TEST_INT_VALUE);
}

TEST_F(SignalInt6MaxSlots, DisconnectAllSlots)
{
  EXPECT_TRUE(signal.connect<&globalFunction>());
  EXPECT_TRUE(signal.connect<&Base0::staticMember>());
  EXPECT_TRUE(signal.notify(3));
  signal.disconnectAll();
  EXPECT_TRUE(signal.isEmpty());
}

TEST_F(SignalInt6MaxSlots, DisconnectSlot)
{
  EXPECT_TRUE(signal.connect<&globalFunction>());
  EXPECT_TRUE(signal.connect<&Base0::staticMember>());
  EXPECT_TRUE(signal.notify(4));
  EXPECT_TRUE(TestGlobal);
  EXPECT_TRUE(TestStatic);
  EXPECT_EQ(argGlobal, 4);
  EXPECT_EQ(argStatic, 4);
  signal.disconnect<&globalFunction>();
  TestGlobal = false;
  TestStatic = false;
  EXPECT_TRUE(signal.notify(5));
  EXPECT_TRUE(TestStatic);
  EXPECT_FALSE(TestGlobal);
  EXPECT_EQ(argGlobal, 4);
  EXPECT_EQ(argStatic, 5);
}
