/*******************************************************************************
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Ernesto Cruz Olivera (ecruzolivera@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 *all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

/**
 ******************************************************************************
 * @file    Test_DmaBuffer.cpp
 * @author  Ernesto Cruz Olivera
 * @version 1.0
 * @date    Oct 9, 2015
 * @brief   Test for a uint16_t DmaBuffer Class
 ******************************************************************************
 */
#include <gtest/gtest.h>
#include <stdint.h>

#include <cstring>

#include "DmaBuffer.hpp"
using namespace eHSM;
class DmaBufferInt16Type : public ::testing::Test
{
 public:
  typedef int16_t                               BufferType;
  static const size_t                           DmaBuffer_LEN = 15;
  Declare::DmaBuffer<BufferType, DmaBuffer_LEN> lBuffer;
};

TEST_F(DmaBufferInt16Type, EmptyAfterCreation)
{
  EXPECT_TRUE(lBuffer.isEmpty());
}

TEST_F(DmaBufferInt16Type, NotFullAfterCreation)
{
  EXPECT_FALSE(lBuffer.isFull());
}

TEST_F(DmaBufferInt16Type, InsertOneItemAndRetrieveIt)
{
  BufferType  item          = 10000;
  size_t      items_written = lBuffer.write(item);
  BufferType *data          = lBuffer.data();
  EXPECT_EQ(items_written, 1);
  EXPECT_EQ(item, *data);
}

TEST_F(DmaBufferInt16Type, InsertOneItemAndCheckNotEmptyAndNotFull)
{
  BufferType item = 55;
  EXPECT_EQ(1u, lBuffer.write(item));
  EXPECT_FALSE(lBuffer.isEmpty());
  EXPECT_FALSE(lBuffer.isFull());
}

TEST_F(DmaBufferInt16Type, InsertSeveralItemsAndRetrieveThem)
{
  BufferType     items[]   = {55, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const uint32_t ITEMS_LEN = 10;
  BufferType *   ret_items = lBuffer.data();
  EXPECT_EQ(ITEMS_LEN, lBuffer.write(items, ITEMS_LEN));
  EXPECT_EQ(0, memcmp(items, ret_items, ITEMS_LEN));
}

TEST_F(DmaBufferInt16Type, InsertUnTilFullAndCheckFullAndNotEmpty)
{
  size_t     tempDmaBuffer_LEN = DmaBuffer_LEN;
  uint32_t   ret_items         = 0;
  BufferType items[]           = {1, 2, 3, 4, 5};
  while(ret_items < tempDmaBuffer_LEN)
  {
    ret_items += lBuffer.write(items, sizeof(items) / sizeof(BufferType));
  }
  EXPECT_TRUE(lBuffer.isFull());
  EXPECT_FALSE(lBuffer.isEmpty());
  EXPECT_EQ(tempDmaBuffer_LEN, ret_items);
}
