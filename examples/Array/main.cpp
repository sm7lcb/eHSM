#include <stdio.h>

#include <cstdint>

#include "Array.hpp"
using namespace std;

void FillArray(eHSM::Array<uint8_t> *localArrayPtr, uint8_t value)
{
  while(!localArrayPtr->isFull())
  {
    localArrayPtr->append(value);
  }
}

bool CheckArray(const eHSM::Array<uint8_t> *localArrayPtr, uint8_t value)
{
  for(uint32_t index = 0; index < localArrayPtr->size(); index++)
  {
    if(localArrayPtr->at(index) != value)
    {
      return false;
    }
  }
  return true;
}

int main()
{
  const uint8_t VALUE_TO_FILL1 = 55;
  const uint8_t VALUE_TO_FILL2 = 99;
  // are actually different type of classes, Array<uint8_t, 16> and
  // Array<uint8_t, 32>
  eHSM::Declare::Array<uint8_t, 16> uint8x16Array;
  eHSM::Declare::Array<uint8_t, 32> uint8x32Array;
  // Nevertheless You can use the same pointer (Array<uint8_t> *) to both
  // classes
  FillArray(&uint8x16Array, VALUE_TO_FILL1);
  FillArray(&uint8x32Array, VALUE_TO_FILL2);

  if(CheckArray(&uint8x16Array, VALUE_TO_FILL1))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }

  if(CheckArray(&uint8x32Array, VALUE_TO_FILL2))
  {
    printf("Ok\r\n");
  }
  else
  {
    printf("FAIL\r\n");
  }
}
