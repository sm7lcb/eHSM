#include <stdio.h>

#include <cstdint>

#include "Signal.hpp"

using namespace std;

class MyClass
{
 public:
  MyClass() {}
  void method()
  {
    printf("MyClass Method called\r\n");
  }
  static void staticMethod()
  {
    printf("MyClass Static method called\r\n");
  }
  void methodWithParameter(int param)
  {
    printf("MyClass Parameter %i called\r\n", param);
  }
};

class MyClass2
{
 public:
  MyClass2() {}
  void method()
  {
    printf("MyClass2 Method called\r\n");
  }
  static void staticMethod()
  {
    printf("MyClass2 Static method called\r\n");
  }
  void methodWithParameter(int param)
  {
    printf("MyClass2 Parameter %i called\r\n", param);
  }
};

int main()
{
  MyClass   obj;
  MyClass2  obj2;
  const int SIGNAL_SLOTS   = 4;
  const int SIGNAL_SLOTS_2 = 2;

  eHSM::Signal<void, SIGNAL_SLOTS> signalVoid;
  signalVoid.connect<MyClass, &MyClass::method>(&obj);
  signalVoid.connect<MyClass::staticMethod>();

  signalVoid.connect<MyClass2, &MyClass2::method>(&obj2);
  signalVoid.connect<MyClass2::staticMethod>();
  signalVoid.notify();

  eHSM::Signal<int, SIGNAL_SLOTS_2> signalInt;
  signalInt.connect<MyClass, &MyClass::methodWithParameter>(&obj);
  signalInt.connect<MyClass2, &MyClass2::methodWithParameter>(&obj2);
  signalInt.notify(5);
}
