#include <stdio.h>

#include "HierarchicalStateMachine.hpp"

using namespace eHSM;

const int STATE_MAX_EVENTS = 2;

void OnPushedInLocked(int event)
{
  printf("Event: %i received\r\n", event);
  printf("Autotransition Locked->Locked\r\n");
}

void OnPushedInUnLocked(int event)
{
  printf("Event: %i received\r\n", event);
  printf("Transition UnLocked->Locked\r\n");
}

void OnCoinInsertedInLocked(int event)
{
  printf("Event: %i received\r\n", event);
  printf("Transition Locked->UnLocked\r\n");
}

void OnCoinInsertedInUnLocked(int event)
{
  printf("Event: %i received\r\n", event);
  printf("Autotransition UnLocked->UnLocked\r\n");
}

int main()
{
  enum Events
  {
    CoinInserted,
    BarPushed
  };
  Declare::State<STATE_MAX_EVENTS> locked;
  Declare::State<STATE_MAX_EVENTS> unlocked;
  // actionHolder is a stack allocated Delegate that will be used to add actions to every events
  State::Action_t actionHolder;
  actionHolder.bind<OnPushedInLocked>();
  locked.addEvent(BarPushed, locked, actionHolder);
  actionHolder.bind<OnCoinInsertedInLocked>();
  locked.addEvent(CoinInserted, unlocked, actionHolder);

  actionHolder.bind<OnCoinInsertedInUnLocked>();
  unlocked.addEvent(CoinInserted, unlocked, actionHolder);
  actionHolder.bind<OnPushedInUnLocked>();
  unlocked.addEvent(BarPushed, locked, actionHolder);

  HierarchicalStateMachine hsm;
  hsm.setInitialState(locked);
  hsm.start();
  hsm.dispatch(BarPushed);
  hsm.dispatch(BarPushed);
  hsm.dispatch(CoinInserted);
  hsm.dispatch(CoinInserted);
  hsm.dispatch(BarPushed);
}
